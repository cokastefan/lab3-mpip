package com.example.stefan.lab3

/**
 * Created by stefan on 28.11.17.
 */
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by stefan on 12.11.17.
 */
@Entity
data class Rating(
        @SerializedName("Source")
        var source: String,
        @SerializedName("Value")
        var value: String,
        @PrimaryKey(autoGenerate = true)
        var id: String
)