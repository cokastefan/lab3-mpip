package com.example.stefan.lab3

import android.arch.persistence.room.*


/**
 * Created by stefan on 28.11.17.
 */
@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movie: Movie)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(movies: Array<out Movie>)

    @Update
    fun updateMovies(vararg movies: Movie)

    @Delete
    fun deleteMovies(vararg movies: Movie)

    @Query("DELETE from movies WHERE imdbId = :arg0")
    fun deleteMovieById(id: String)

    @Query("SELECT * FROM movies")
    fun loadAllMovies(): List<Movie>

    @Query("SELECT * FROM movies WHERE imdbId = :arg0")
    fun findMovieById(p0: String): List<Movie>

    @Query("DELETE FROM movies")
    fun nuke()
}
