package com.example.stefan.lab3;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.SharedPreferences;

/**
 * Created by stefan on 29.11.17.
 */

public class App extends Application {
    private static final String DATABASE_NAME = "MyDatabase";
    private static final String PREFERENCES = "RoomDemo.preferences";
    private static final String KEY_FORCE_UPDATE = "force_update";
    public static App INSTANCE;
    private AppDatabase database;

    public static App get() {
        if (INSTANCE == null) {
        }
        return INSTANCE;
    }

    @Override
    public void onCreate() {

        // create database
        database = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .build();

        INSTANCE = this;
        super.onCreate();

    }

    public AppDatabase getDB() {
        return database;
    }

    public boolean isForceUpdate() {
        return getSP().getBoolean(KEY_FORCE_UPDATE, true);
    }

    public void setForceUpdate(boolean force) {
        SharedPreferences.Editor edit = getSP().edit();
        edit.putBoolean(KEY_FORCE_UPDATE, force);
        edit.apply();
    }

    private SharedPreferences getSP() {
        return getSharedPreferences(PREFERENCES, MODE_PRIVATE);
    }

}
