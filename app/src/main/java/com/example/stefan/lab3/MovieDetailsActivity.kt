package com.example.stefan.lab3


import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.ShareActionProvider
import android.util.Log
import android.view.Menu
import com.example.stefan.lab3.databinding.ActivityMovieDetailsBinding
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_movie_details.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL

class MovieDetailsActivity : AppCompatActivity() {
    private lateinit var mMovie: Movie
    private lateinit var mBinding: ActivityMovieDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        mMovie = intent.getParcelableExtra("movie")

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_movie_details)
        if (checkConnectivity()) {
            getDetails()
        }
        mBinding.setVariable(BR.movie, mMovie)

    }

    private lateinit var mShareActionProvider: ShareActionProvider

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate menu resource file.
        menuInflater.inflate(R.menu.share, menu)

        // Locate MenuItem with ShareActionProvider
        val item = menu.findItem(R.id.menu_item_share)

        // Fetch and store ShareActionProvider
        mShareActionProvider = MenuItemCompat.getActionProvider(item) as ShareActionProvider

        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        val shareBodyText = "Movie website: ${mMovie.website}"
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mMovie.name)
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText)

        mShareActionProvider.setShareIntent(sharingIntent)

        // Return true to display menu
        return true
    }

    private fun getDetails() {
        doAsync {
            val result = URL("http://www.omdbapi.com/?i=${mMovie.imdbId}&plot=long&apikey=cb0c41&r=json").readText()
            uiThread {
                writeDetails(result)
            }
        }
    }

    private fun writeDetails(result: String) {
        mMovie = Gson().fromJson(result, Movie::class.java)
        mBinding.setVariable(BR.movie, mMovie)
        big_image.loadUrl(mMovie.imageUrl)
        Log.d("Movie", mMovie.toString())
    }

    override fun onBackPressed() {
        alert("Do you want to go back?") {
            title("Back")
            yesButton { super.onBackPressed() }
            noButton { }
        }.show()
    }

    private fun checkConnectivity(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return cm.activeNetworkInfo != null

    }
}
