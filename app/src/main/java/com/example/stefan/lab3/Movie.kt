package com.example.stefan.lab3

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by stefan on 12.11.17.
 */
@Entity(tableName = "movies")
data class Movie(
        @SerializedName("Title")
        var name: String = "",
        @SerializedName("Year")
        var year: String = "",
        @SerializedName("imdbID")
        var imdbId: String = "",
        @SerializedName("Type")
        var type: String = "",
        @SerializedName("Poster")
        var imageUrl: String = "",
        @SerializedName("Plot")
        var description: String = "",
        @SerializedName("Director")
        var director: String = "",
        @SerializedName("Rated")
        var rated: String = "",
        @SerializedName("Released")
        var released: String = "",
        @SerializedName("Runtime")
        var runtime: String = "",
        @SerializedName("Genre")
        var genre: String = "",
        @SerializedName("Writer")
        var writer: String = "",
        @SerializedName("Actors")
        var actors: String = "",
        @SerializedName("Language")
        var language: String = "",
        @SerializedName("Country")
        var country: String = "",
        @SerializedName("Awards")
        var awards: String = "",
//        @SerializedName("Ratings")
//        var ratings: List<Rating>,
        @SerializedName("Metascore")
        var metascore: String = "",
        @SerializedName("imdbRating")
        var imdbRating: String = "",
        @SerializedName("imdbVotes")
        var imdbVotes: String = "",
        @SerializedName("DVD")
        var dvd: String = "",
        @SerializedName("BoxOffice")
        var boxOffice: String = "",
        @SerializedName("Production")
        var production: String = "",
        @SerializedName("Website")
        var website: String = ""
) : Parcelable {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
        id = parcel.readLong()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(year)
        parcel.writeString(imdbId)
        parcel.writeString(type)
        parcel.writeString(imageUrl)
        parcel.writeString(description)
        parcel.writeString(director)
        parcel.writeString(rated)
        parcel.writeString(released)
        parcel.writeString(runtime)
        parcel.writeString(genre)
        parcel.writeString(writer)
        parcel.writeString(actors)
        parcel.writeString(language)
        parcel.writeString(country)
        parcel.writeString(awards)
        parcel.writeString(metascore)
        parcel.writeString(imdbRating)
        parcel.writeString(imdbVotes)
        parcel.writeString(dvd)
        parcel.writeString(boxOffice)
        parcel.writeString(production)
        parcel.writeString(website)
        parcel.writeLong(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Movie> {
        override fun createFromParcel(parcel: Parcel): Movie {
            return Movie(parcel)
        }

        override fun newArray(size: Int): Array<Movie?> {
            return arrayOfNulls(size)
        }
    }
}