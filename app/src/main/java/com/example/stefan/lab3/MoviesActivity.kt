package com.example.stefan.lab3

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.widget.SearchView
import kotlinx.android.synthetic.main.activity_movies.*


class MoviesActivity : AppCompatActivity() {
    private lateinit var menu: Menu
    private lateinit var movies: List<Movie>
    private lateinit var movieDetails: MutableList<Movie>
    private lateinit var adapter: MoviesAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)

        movies = emptyList()

        val database = App.get()

        movieDetails = database.db.movieDao().loadAllMovies() as MutableList<Movie>

        list.layoutManager = LinearLayoutManager(this)

        adapter = MoviesAdapter(movieDetails) {
            val intent = Intent(this@MoviesActivity, MovieDetailsActivity::class.java)
            intent.putExtra("movie", it)
            startActivity(intent)
        }

        list.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search, menu)

        this.menu = menu

        val manager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val search = menu.findItem(R.id.action_search).actionView as SearchView

        search.setSearchableInfo(manager.getSearchableInfo(componentName))

        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(query: String?): Boolean {
//                if (query != null) {
//                    loadData(query)
//                }
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    loadData(query)
                }
                return true
            }
        })

        return true
    }

    override fun onNewIntent(intent: Intent?) {
        if (intent != null) {
            if (intent.action == MoviesService.ACTION_OUT) {
//                val result = intent.getStringExtra(MoviesService.EXTRA_PARAM2)
//                readJson(result)
                movies = App.get().db.movieDao().loadAllMovies()
                getDetails()
            } else if (intent.action == MoviesService.ACTION_OUT_DETAILS) {
//                val result = intent.getStringExtra(MoviesService.EXTRA_PARAM2)
//                addDetails(result)
                movieDetails.clear()
                val db = App.get().db.movieDao().loadAllMovies() as MutableList<Movie>
                movieDetails.addAll(db)
                list.adapter.notifyDataSetChanged()
            }
        }
        list.adapter.notifyDataSetChanged()

        super.onNewIntent(intent)
    }

    private fun getDetails() {
        for (movie in movies) {
            val intent = Intent(this, MoviesService::class.java)
            intent.action = MoviesService.ACTION_DETAILS
            intent.putExtra(MoviesService.EXTRA_PARAM1, movie.imdbId)
            startService(intent)
        }
    }

    private fun loadData(query: String) {
        App.get().db.movieDao().nuke()
        val intent = Intent(this, MoviesService::class.java)
        intent.action = MoviesService.ACTION_SEARCH
        intent.putExtra(MoviesService.EXTRA_PARAM1, query)
        startService(intent)
    }

}
