package com.example.stefan.lab3

import android.app.IntentService
import android.content.Context
import android.content.Intent
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import org.jetbrains.anko.toast
import java.net.URL

/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 *
 *
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
class MoviesService : IntentService("MoviesService") {

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            if (intent.action == ACTION_SEARCH) {
                val param1 = intent.getStringExtra(EXTRA_PARAM1)
                handleSearch(param1)
            } else if (intent.action == ACTION_DETAILS) {
                val param1 = intent.getStringExtra(EXTRA_PARAM1)
                handleDetails(param1)
            }
        }
    }

    private fun handleDetails(imdbId: String?) {
        val database = App.get().db.movieDao()
        val result = URL("http://www.omdbapi.com/?i=$imdbId&plot=short&apikey=cb0c41&r=json").readText()

        val newMovie: Movie = Gson().fromJson(result, Movie::class.java)
        database.deleteMovieById(imdbId!!)
        database.insertMovie(newMovie)

        val intent = Intent(applicationContext, MoviesActivity::class.java)
        intent.action = ACTION_OUT_DETAILS
        startActivity(intent)
    }

    private fun readJson(result: String?) {
        val parser = JsonParser()
        val json: JsonElement = parser.parse(result)
        val resultJson: JsonObject = json.asJsonObject
        val resultArray = resultJson.get("Search")

        if (resultArray == null) {
            toast("No results found")
            return
        }
        val movies: Array<Movie>
        movies = Gson().fromJson(resultArray, object : TypeToken<Array<Movie>>() {}.type)

        App.get().db.movieDao().insertAll(movies = movies)
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private fun handleSearch(movie: String) {
        val result = URL("http://www.omdbapi.com/?s=$movie&apikey=cb0c41&r=json").readText()
        val intent = Intent(applicationContext, MoviesActivity::class.java)
        intent.action = ACTION_OUT
        intent.putExtra(EXTRA_PARAM2, result)
        readJson(result)
        startActivity(intent)
//        getDetails()
    }

    private fun getDetails() {
        val movies = App.get().db.movieDao().loadAllMovies()
        for (movie in movies) {
            handleDetails(movie.imdbId)
        }
        val intent = Intent(applicationContext, MoviesActivity::class.java)
        intent.action = ACTION_OUT_DETAILS
        startActivity(intent)
    }

    companion object {
        // TODO: Rename actions, choose action names that describe tasks that this
        // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
        val ACTION_SEARCH = "com.example.stefan.lab3.action.FOO"
        val ACTION_DETAILS = "com.example.stefan.lab3.action.BAZ"

        // TODO: Rename parameters
        val EXTRA_PARAM1 = "query"
        val ACTION_OUT = "com.example.broadcast.SEARCH_COMPLETE"
        val ACTION_OUT_DETAILS = "com.example.broadcast.DETAILS"
        val EXTRA_PARAM2 = "com.example.stefan.lab3.extra.PARAM2"

        /**
         * Starts this service to perform action Foo with the given parameters. If
         * the service is already performing a task this action will be queued.
         *
         * @see IntentService
         */
        // TODO: Customize helper method
        fun startActionFoo(context: Context, param1: String, param2: String) {
            val intent = Intent(context, MoviesService::class.java)
            intent.action = ACTION_SEARCH
            intent.putExtra(EXTRA_PARAM1, param1)
            intent.putExtra(EXTRA_PARAM2, param2)
            context.startService(intent)
        }

        /**
         * Starts this service to perform action Baz with the given parameters. If
         * the service is already performing a task this action will be queued.
         *
         * @see IntentService
         */
        // TODO: Customize helper method
        fun startActionBaz(context: Context, param1: String, param2: String) {
            val intent = Intent(context, MoviesService::class.java)
            intent.action = ACTION_DETAILS
            intent.putExtra(EXTRA_PARAM1, param1)
            intent.putExtra(EXTRA_PARAM2, param2)
            context.startService(intent)
        }
    }
}
